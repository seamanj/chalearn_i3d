import xmltodict
import argparse
import os
import pickle5 as pickle
import gzip
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import math
import cv2

from pathlib import Path
import torch
import sys
sys.path.append('.')
from i3d import *
import shutil
from utils.imutils import *
from utils.transforms import *
from zsvision.zs_utils import BlockTimer


OnGPU = True
local = True

# = tj : https://discuss.pytorch.org/t/does-dataparallel-matters-in-cpu-mode/7587/4
class WrappedModel(torch.nn.Module):
    def __init__(self, module):
        super(WrappedModel, self).__init__()
        self.module = module  # that I actually define.

    def forward(self, x):
        return self.module(x)




def load_model(
        checkpoint_path: Path,
        num_classes: int,
        num_in_frames: int,
) -> torch.nn.Module:
    """Load pre-trained I3D checkpoint, put in eval mode.  Download checkpoint
    from url if not found locally.
    """

    model = InceptionI3d(
        num_classes=num_classes,
        spatiotemporal_squeeze=True,
        final_endpoint="Logits",
        name="inception_i3d",
        in_channels=3,
        dropout_keep_prob=0.5,
        num_in_frames=num_in_frames,
        include_embds=True
    )

    if OnGPU:
        print('loading model on GPU.')
        model = torch.nn.DataParallel(model).cuda()
        checkpoint = torch.load(checkpoint_path)
    else:
        print('loading model on CPU.')
        model = WrappedModel(model)
        checkpoint = torch.load(checkpoint_path, map_location=torch.device('cpu'))

    model.module.load_state_dict(checkpoint["model_state"])
    model.eval()
    return model

def prepare_input(
    rgb: torch.Tensor,
    resize_res: int = 256,
    inp_res: int = 224,
    mean: torch.Tensor = 0.5 * torch.ones(3), std=1.0 * torch.ones(3),
):
    """
    Process the video:
    1) Resize to [resize_res x resize_res]
    2) Center crop with [inp_res x inp_res]
    3) Color normalize using mean/std
    """
    iC, iF, iH, iW = rgb.shape
    rgb_resized = np.zeros((iF, resize_res, resize_res, iC))
    for t in range(iF):
        tmp = rgb[:, t, :, :]
        tmp = resize_generic(
            im_to_numpy(tmp), resize_res, resize_res, interp="bilinear", is_flow=False
        )
        rgb_resized[t] = tmp
    rgb = np.transpose(rgb_resized, (3, 0, 1, 2))
    # Center crop coords
    ulx = int((resize_res - inp_res) / 2)
    uly = int((resize_res - inp_res) / 2)
    # Crop 256x256
    rgb = rgb[:, :, uly : uly + inp_res, ulx : ulx + inp_res]
    rgb = to_torch(rgb).float()
    assert rgb.max() <= 1
    rgb = color_normalize(rgb, mean, std)
    return rgb


def load_rgb_video(video_path: Path, frame_range: range, fps: int) -> torch.Tensor:
    """
    Load frames of a video using cv2 (fetch from provided URL if file is not found
    at given location).
    """

    cap = cv2.VideoCapture(str(video_path))
    cap_height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    cap_width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    cap_fps = cap.get(cv2.CAP_PROP_FPS)
    num_video_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))


    assert cap_fps == fps, f"ffmpeg failed to produce a video at {fps}"
    # # cv2 won't be able to change frame rates for all encodings, so we use ffmpeg
    # if cap_fps != fps:
    #     tmp_video_path = f"{video_path}.tmp.{video_path.suffix}"
    #     shutil.move(video_path, tmp_video_path)
    #     cmd = (f"ffmpeg -i {tmp_video_path} -pix_fmt yuv420p "
    #            f"-filter:v fps=fps={fps} {video_path}")
    #     print(f"Generating new copy of video with frame rate {fps}")
    #     os.system(cmd)
    #     Path(tmp_video_path).unlink()
    #     cap = cv2.VideoCapture(str(video_path))
    #     cap_height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    #     cap_width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    #     cap_fps = cap.get(cv2.CAP_PROP_FPS)
    #     assert cap_fps == fps, f"ffmpeg failed to produce a video at {fps}"


    f = 0
    rgb = []
    cap.set(cv2.CAP_PROP_POS_FRAMES, frame_range[0])
    for i in frame_range:
        ret, frame = cap.read()
        if not ret:
            break
        # BGR (OpenCV) to RGB (Torch)
        frame = frame[:, :, [2, 1, 0]]
        rgb_t = im_to_torch(frame)
        rgb.append(rgb_t)
        f += 1
    cap.release()
    # (nframes, 3, cap_height, cap_width) => (3, nframes, cap_height, cap_width)
    rgb = torch.stack(rgb).permute(1, 0, 2, 3)



    print(f"Loaded video {video_path} with {f} frames [{cap_height}hx{cap_width}w] res. "
          f"at {cap_fps}")
    return rgb

def pad_input(rgb: torch.Tensor,
               num_in_frames: int):
    C, nFrames, H, W = rgb.shape


    rgb = torch.cat((torch.zeros(C, num_in_frames // 2 - 1, H, W), rgb), dim=1) # pad the beginning
    rgb = torch.cat((rgb, torch.zeros(C, num_in_frames // 2, H, W)), dim=1) # pad the ending

    return rgb

def sliding_windows(
        rgb: torch.Tensor,
        num_in_frames: int,
        stride: int,
) -> tuple:
    """
    Return sliding windows and corresponding (middle) timestamp
    """
    C, nFrames, H, W = rgb.shape
    # If needed, pad to the minimum clip length
    if nFrames < num_in_frames:
        rgb_ = torch.zeros(C, num_in_frames, H, W)
        rgb_[:, :nFrames] = rgb
        rgb_[:, nFrames:] = rgb[:, -1].unsqueeze(1)
        rgb = rgb_
        nFrames = rgb.shape[1]

    num_clips = math.ceil((nFrames - num_in_frames) / stride) + 1
    plural = ""
    if num_clips > 1:
        plural = "s"
    print(f"{num_clips} clip{plural} resulted from sliding window processing.")

    rgb_slided = torch.zeros(num_clips, 3, num_in_frames, H, W)
    t_mid = []
    # For each clip
    for j in range(num_clips):
        # Check if num_clips becomes 0
        actual_clip_length = min(num_in_frames, nFrames - j * stride)
        if actual_clip_length == num_in_frames:
            t_beg = j * stride
        else:
            t_beg = nFrames - num_in_frames
        t_mid.append(t_beg + num_in_frames / 2)
        rgb_slided[j] = rgb[:, t_beg : t_beg + num_in_frames, :, :]
    return rgb_slided, np.array(t_mid)



def make_dir(dir: str) -> str:
    if not os.path.isdir(dir):
        os.makedirs(dir)
    return dir

def load_gklz(filename):
    with gzip.open(filename, "rb") as f:
        loaded_object = pickle.load(f)
        return loaded_object


def save_gklz(obj, filename):
    with gzip.open(filename, "wb") as f:
        pickle.dump(obj, f, -1)

def main(params):

    input_file = params.input_file
    output_root = params.output_root



    file_name = os.path.splitext(input_file.split('/')[-1])[0]

    # relative_dir = os.path.relpath(video_path, video_root)
    # sub_dir = os.path.join(*relative_dir.split('/')[:-1])
    # output_dir = os.path.join(output_root, sub_dir)



    output_file = os.path.join(output_root, "{}.gklz".format(file_name))


    if os.path.exists(output_file):
        print('{} already exists'.format(output_file))
        return



    # tj : count frame number
    vidcap = cv2.VideoCapture(input_file)
    num_video_frames = int(vidcap.get(cv2.CAP_PROP_FRAME_COUNT))
    if num_video_frames <= 0:
        print('video has zero frame')
        return

    num_in_frames = 16

    frame_range = range(num_video_frames)

    bslcp_signbank_vocab_path = '/home/seamanj/Workspace/BSLCP/parsed_bslcp_signbank_vocab.gklz' if local else '/vol/research/extol/tmp/Tao_BSLCP/parsed_bslcp_signbank_vocab.gklz'
    bslcp_signbank_vocab = load_gklz(bslcp_signbank_vocab_path)


    num_classes = len(bslcp_signbank_vocab['words'])


    with BlockTimer("Loading model"):
        model = load_model(
            # checkpoint_path=Path(),
            checkpoint_path=Path("/home/seamanj/Workspace/Skeletor/models_condor/exp179/best.ckpt" if local else "/vol/research/SignPose/tj/Workspace/Skeletor_New/exp181/models/exp179/best.ckpt"),
            num_classes=num_classes,
            num_in_frames=num_in_frames
        )

    features = []
    with BlockTimer("Loading video frames {}\n".format(list(frame_range))):


        rgb_orig = load_rgb_video(
            video_path=Path(input_file),
            frame_range=frame_range,
            fps=30
        )

        # Prepare: resize/crop/normalize
        rgb_input = prepare_input(rgb_orig)

        padded_input = pad_input(rgb_input, num_in_frames=num_in_frames)

        # Sliding window
        rgb_slides, t_mid = sliding_windows(
            rgb=padded_input,
            stride=1,
            num_in_frames=num_in_frames,
        )
        num_clips = rgb_slides.shape[0]
        batch_size = 1
        num_batches = math.ceil(num_clips / batch_size)

        for b in range(num_batches):
            inp = rgb_slides[b * batch_size: (b + 1) * batch_size]
            # Forward pass
            out = model(inp)
            # raw_scores = np.append(raw_scores, out["logits"].cpu().detach().numpy(), axis=0)
            features.append(out["embds"].cpu().detach().numpy())

    features = np.vstack(features)
    features = features.squeeze(2).squeeze(2).squeeze(2)

    assert features.shape[0] == num_video_frames, 'features.shape[0] != num_video_frames'

    save_data = features

    make_dir(output_root)
    save_gklz(obj=save_data, filename=output_file)
    print('{} saved'.format(output_file))

def load_data():
    # a = load_gklz("/home/seamanj/Workspace/SignBank/i3d_skeletor179_exp22/ABOUT_ABOUT_0.gklz")
    a = load_gklz("/home/seamanj/Workspace/ChaLearn_I3D/i3d_bsl1k/signer0_sample1_color.gklz")

    # a = pickle.load(open('/home/seamanj/Workspace/BSLCP/bslcp_words.pkl', "rb"))

    print('1')

if __name__ == "__main__":

    load_data()

    # Assumes they are in the same order
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--input_file",
        type=str,
        default="/home/seamanj/Workspace/ChaLearn_I3D/signer0_sample1_color.mp4",
    )


    parser.add_argument(
        "--output_root",
        type=str,
        default="/home/seamanj/Workspace/ChaLearn_I3D/i3d_bsl1k" if local else '/vol/research/extol/tmp/Tao_SignBank/i3d_skeletor181_exp22'
    )
    params, _ = parser.parse_known_args()

    main(params)
