#!/bin/bash

cd /vol/research/SignPose/tj/Workspace/wordspottingnew/exp14
/vol/research/SignPose/tj/Software/miniconda3/envs/BERT_skeleton/bin/python -m src --mode=train --config_path=./configs/exp14/config.yaml